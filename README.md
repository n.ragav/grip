# Effortless markdown with grip

This repistory encapsulates the open source markdown rendering tool
[Grip](https://github.com/joeyespo/grip) in a lightweight
[distroless](https://github.com/GoogleContainerTools/distroless/blob/master/experimental/python3/README.md)
docker image.

Grip is a popular Python-based tool for rendering markdowns in a browser. While
there are several tools such as [Markdown Live
Preview](https://markdownlivepreview.com/), which lets you edit markdown in a
browser offering split view of the rendered page, and
[Dillinger](https://dillinger.io/), which additionaly is also
available as an [installable version](https://github.com/joemccann/dillinger), I
like grip for two reasons:

1. It lets you use the editor of your choice (I won't give up my Vim for any
   reason).
2. It's a lightweight process that could be run in a small container, and be
   thrown away after the job is done.

**P.S.**: I know vscode
[supports](https://code.visualstudio.com/docs/languages/markdown) markdown out
of the box, but I like the simplicity of Vim very much, that I
don't use any other editor (you may download and use my
[vimrc](https://gitlab.com/n.ragav/vimrc) if you'd like).

## Prerequisite

You need to have Docker installed on your machine, the details of which is out
of scope for this document. You may follow the [official
manual](https://docs.docker.com/engine/install/ubuntu/) for information
regarding docker installation.

## Quickstart

### Method 1

Run the following command from the directory where you have your markdown
README.md This would start grip in a docker container which will render the
README.md on your current directory on [localhost:8000](http://localhost:8000).
Once the editing is complete, you can hit `CTRL+C` to terminate the container.

```bash
docker run -p 8000:6419 -v $(pwd):/home/grip ragavan/grip "0.0.0.0:6419"
```

**Note**:  If you are worried what is inside this image, you may examine its
contents [here](./builder/grip.dockerfile).

### Method 2

Alternatively, you could also download and run the companion script
[run-grip.sh](./run-grip.sh) as shown below, with or without a filename.  When
you don't specify a filename, grip looks for README.md in the directory from
which this script is run. Otherwise, it renders the file specified by the name.

```bash
./run-grip.sh [<filename>]
```

**Note**: Grip uses the [GitHub markdown
API](https://docs.github.com/en/rest/reference/markdown) to render the files.
Running grip without a token has a hourly API limit. To avoid hitting this
limit, you can [get your personal access
token](https://github.com/settings/tokens) from GitHub (select no scope), create
a file in the location `~/.grip/settings.py` with the following data.

```python
HOST = '0.0.0.0'
PASSWORD = 'your token' # Enter your token from github here.
```

Once the job is done, hit `CTRL+C` to terminate this script.  One advantage of
using this script over the plain docker command is that, this script also takes
care of cleaning up the docker processi, and any cached files.

### Method 3

Optionally, you can also add an entry to `~/.bashrc` to create an alias. To do
this, open ~/.bashrc and add the following function to the end of it.

```bash
function grip() {
    <directory where you cloned this repo>/run-grip.sh $1
}
```

After this, when you restart your terminal, you can run it by simply typing
`grip` in the command prompt.

## Building it yourself

If you don't want to use the image from my  docker repository
[ragavan/grip](https://hub.docker.com/r/ragavan/grip) for
my whatever reasons, you can build your own image and push it to your own
repository. For this purpose, you will need to setup docker login etc., the
details of which are beyond the scope of this document.

In order to build it yourself, you need to clone this repository and run a few
commands. The details are as follows:

```bash
git clone https://gitlab.com/n.ragav/grip.git
cd grip/builder
./build.sh <your-dockerhub-username>
```

If you examined the contents of [grip.dockerfile](./builder/grip.dockerfile)
closely, you would notice that it uses for its base image a Python builder image
from my repository, namely, `ragavan/pyenv-python:<version>`, and for the
runtime distroless image it uses the image from Google available at
[gcr.io/distroless/python3](https://console.cloud.google.com/gcr/images/distroless/GLOBAL/python3).
