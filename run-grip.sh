#!/bin/bash

set -e

if [ $# -eq 1 ]; then
    filename=$1
    if [[ ! $filename =~ ^.*\.md$ ]]; then
        echo "Invalid argument for filename." \
             "Grip will look for README.md"
        filename="."
    else
        echo "Grip will look for a file namely $filename"
    fi
fi

mkdir -p ${HOME}/.grip
ls ${HOME}/.grip/settings.py 2> /dev/null || \
    echo "HOST = '0.0.0.0'" > ${HOME}/.grip/settings.py

echo "-------------------------------------------"
echo "Grip will now start!"
echo "Point your browser at http://localhost:8000"
echo "-------------------------------------------"

docker run --rm -p 8000:6419 \
    -v $(pwd):/home/grip \
    -v ${HOME}/.grip:/home/grip/.grip \
    ragavan/grip $filename

rm -rf .grip/
