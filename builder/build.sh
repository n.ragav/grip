#!/bin/bash
# author    Ragav N

set -eo pipefail

script=$(basename $0)
script_dir=$(echo $0 | sed 's/\(^.*\)\/[^/]*$/\1/')

# This should be your dockerhub username, this is where the built grip image
# will be pushed.
if [ $# -ne 1 ]; then
    echo "Usage: ./$script <dockerhub-username>"
    exit 1
fi

username=$1

# This is the latest Python distroless repo managed by Google
gcr_repo="gcr.io/distroless/python3-debian10"

# Get the version of Python in the latest image from the repo.
gcr_version=$(docker run $gcr_repo "--version" \
    | cut -d' ' -f 2)

echo "Python version in ${gcr_repo} is ${gcr_version}"


# This is dockerhub's public API url to get metadata about repositories.
docker_url='https://registry.hub.docker.com/v2/repositories'

# Pull the builder image.
builder_image="${username}/pyenv-python"

repo_url="${docker_url}/${builder_image}/tags?page_size=1024"

my_versions=($(curl -L -s $repo_url  \
            | jq '."results"[]["name"]' \
            | tr -d '"' | grep "^[0-9]"))

# If none of the builder images has the same version of Python as in the
# gcr.io's distroless image, print a message and abort.
if ! [[ " ${my_versions[@]} " =~ " ${gcr_version} " ]]; then
    echo "Required Python version ${gcr_version} not in ${builder_image}"
    exit 1
fi

# This will be the repo of the final grip image.
image="${username}/grip"

tag_ver="${image}:py-${gcr_version}"
tag_latest="${image}:latest"

echo "Building grip for Python version ${gcr_version}"
docker build --tag $tag_ver --tag $tag_latest \
    --build-arg BUILDER_IMAGE=${builder_image}:${gcr_version} \
    --build-arg DISTROLESS_IMAGE=${gcr_repo} \
    -f ${script_dir}/grip.dockerfile ${script_dir}

docker push $tag_ver
docker push $tag_latest
