ARG BUILDER_IMAGE
ARG DISTROLESS_IMAGE
FROM ${BUILDER_IMAGE} as temp

# Install pip and grip
RUN python -m venv ./venv \
    && ./venv/bin/pip install --upgrade pip \
    && ./venv/bin/pip install grip

USER root
RUN mkdir -p /home/grip

# Build the final image from the distroless version of Python. This is exactly
# the same as the version on gcr.io, except that it is tagged with the exact
# python version that's inside the container.
FROM ${DISTROLESS_IMAGE}
LABEL maintainer="Ragav N"

# Create an user and group namely grip, and gripusers respectively, so that grip
# would run as underprivileged user.
RUN echo "grip:x:1000:1001::/home/grip:/bin/sh" >> /etc/passwd
RUN echo "gripusers:x:1000:grip" >> /etc/group
RUN echo "grip:x:1001:" >> /etc/group

COPY --from=temp /home/pyrunner/venv /venv
COPY --from=temp --chown=grip:gripusers /home/grip /home/grip

# The commands rm and ln are copied temporarily so that symbolic links could be
# created to the location of the python binary.
COPY --from=temp /bin/rm /bin/rm
COPY --from=temp /bin/ln /bin/ln

RUN ln -fs /usr/bin/python /venv/bin/python
RUN rm /bin/ln /bin/rm

USER grip
WORKDIR /home/grip

# Grip runs on this port.
EXPOSE 6419

ENTRYPOINT ["/venv/bin/python",  "-m", "grip"]
